libtie-cphash-perl (2.000-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtie-cphash-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 17:39:56 +0100

libtie-cphash-perl (2.000-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 20:44:08 +0100

libtie-cphash-perl (2.000-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Fri, 08 Jan 2021 02:41:29 +0100

libtie-cphash-perl (2.000-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 2.000
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.6
  * Add autopkgtest-pkg-perl
  * Update year of upstream copyright

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Fri, 14 Aug 2015 15:41:30 -0300

libtie-cphash-perl (1.06-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release.
  * Update years of copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Wed, 25 Dec 2013 20:33:15 +0100

libtie-cphash-perl (1.05-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Added: ${misc:Depends} to Depends: field.
  * debian/control: fix Homepage field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release.
  * Switch to "3.0 (quilt)" source format.
  * Switch to debhelper compatibility level 8. Use tiny debian/rules.
  * debian/copyright: update formatting, license stanzas, years of
    upstream and packaging copyright.
  * d/control: remove unneeded build dependencies, and version from perl
    build dependency.
  * Bump Standards-Version to 3.9.2 (no further changes).
  * Make short description a noun phrase.

 -- gregor herrmann <gregoa@debian.org>  Sat, 21 Jan 2012 15:25:22 +0100

libtie-cphash-perl (1.04-1) unstable; urgency=low

  * debian/control: Changed: Maintainer set to Debian Perl Group <pkg-
    perl-maintainers@lists.alioth.debian.org> (was: Debian Perl Project
    <pkg-perl-maintainers@lists.alioth.debian.org>).

  * New upstream release.
  * debian/watch: use dist-based URL.
  * debian/copyright: update copyright and license statement.
  * debian/control:
    - indent examples in long description
    - add /me to Uploaders
  * Refresh debian/rules, no functional changes; don't install README any
    more.

 -- gregor herrmann <gregoa@debian.org>  Sun, 27 Apr 2008 21:13:36 +0200

libtie-cphash-perl (1.03-2) unstable; urgency=low

  [ Ernesto Hernández-Novich (USB) ]
  * Moved package into Debian Pkg Perl Project SVN.
  * Added Build-Depends on libtest-pod-perl y libtest-pod-coverage-perl.
  * Cleanup debian/rules.
  * Fixed watch file.
  * Wrapped long lines in control file.
  * Fixed Maintainer address in control file.
  * Fixed copyright and control file with a better URL.

  [ Gunnar Wolf ]
  * Bumped up standards-version to 3.7.3 (No changes needed)
  * Bumped up dh-compat version to 5
  * Moved libmodule-build-perl from b-d-i to b-d
  * Removed the generated .packlist files
  * Added myself as an uploader

  [ Ernesto Hernández-Novich (USB) ]
  * Upgraded to debhelper 6

 -- Ernesto Hernández-Novich (USB) <emhn@usb.ve>  Mon, 14 Jan 2008 10:53:09 -0430

libtie-cphash-perl (1.03-1) unstable; urgency=low

  * Update Standards Version.
  * Cleanup debian/rules.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Mon, 02 Jul 2007 12:16:43 -0400

libtie-cphash-perl (1.02-1) unstable; urgency=low

  * Initial Release.

 -- Ernesto Hernández-Novich <emhn@telcel.net.ve>  Wed, 03 May 2006 09:47:54 -0400
